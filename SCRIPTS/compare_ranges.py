# -*- coding: utf-8 -*-
"""
Created on Fri May 21 12:34:16 2021

@author: Aina Ribes
"""


import pandas as pd

homo = pd.read_csv("pdb_pisa_homo_mem_compl_def.csv", delimiter=',')

hetero = pd.read_csv("pdb_pisa_hetero_mem_compl_def.csv", delimiter=',')

#HETEROS
df_hetero = pd.DataFrame({"Region":[], "Prov":[]})
regio = list(df_hetero_mem_def['Regio_crist_1'])
tm = list(df_hetero_mem_def['Segments_TM_1'])
index_hetero_1 = ""
ind_het =[]
compt = -1
for i, element in enumerate(regio):
    r = regio[i]
    tmi = tm[i]
    resultats={range(int(r[0]),int(r[1])): "YES"}
    for j,element in enumerate(tmi):
        tmx =tmi[j].split("..")
        for k, element in enumerate(tmx):
            for rango, resultado in resultats.items():
                if int(tmx[k]) in rango:
                    break
                else:
                    resultado = "NO"
            if resultado == "YES":
                index_hetero_1 = (i)
                ind_het.append(i)
                compt += 1
                if ind_het[compt] != ind_het[compt-1]:
                    pdb_ap_uni = [index_hetero_1, resultado]
                    pdb_series = pd.Series(pdb_ap_uni, index = df_hetero.columns)
                    df_hetero = df_hetero.append(pdb_series, ignore_index=True)
                if compt == 0:
                    pdb_ap_uni = [index_hetero_1, resultado]
                    pdb_series = pd.Series(pdb_ap_uni, index = df_hetero.columns)
                    df_hetero = df_hetero.append(pdb_series, ignore_index=True)

ind_het_1 = list(df_hetero['Region'])
    
df_hetero_mem_1_def = pandas.DataFrame(df_hetero_mem_def, index = ind_het_1)
df_hetero_mem_1_def.index= range(len(df_hetero_mem_1_def))

df_hetero_mem_1_def.to_csv("pdb_pisa_hetero_mem_compl_def_1.csv")  


df_hetero_comp_1 = df_hetero_mem_1_def.drop(['Accession_2', 'Regio_crist_2', 'Segments_TM_2', 'Pfam2'], axis=1)
acc_membrana = list(df_pdb['Accession'])
unip1 = list( df_hetero_comp_1['Accession_1'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)
        
df_pdb_com_het_1 = pandas.DataFrame(df_pdb, index = indexu)
df_pdb_com_het_1.index= range(len(df_pdb_com_het_1))

acc_membrana = list(df_pdb_com_het_1['PDB'])
unip1 = list( df_hetero_comp_1['PDB'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)

df_pdb_com_het_1 = pandas.DataFrame(df_pdb_com_het_1, index = indexu)
df_pdb_com_het_1.index= range(len(df_pdb_com_het_1))

df_pdb_com_het_1 = df_pdb_com_het_1.drop(['ID', 'Gene', 'Organism', 'Method', 'Angstrom'], axis=1)
df_pdb_com_het_1.columns=['Accession', 'PDB', 'Chains', 'Regio_crist'] 

df_hetero_comp_11 = df_hetero_comp_1.merge(df_pdb_com_het_1, on="PDB", how="left")
df_hetero_comp_11 = df_hetero_comp_11.drop(['Accession', 'Regio_crist'], axis=1)

df_hetero_comp_11.columns = ['PDB', 'Accession_1', 'Regio_crist_1', 'Segments_TM_1', 'Pfam1', 'Chains_1']

df_hetero_comp_2 = df_hetero_mem_1_def.drop(['Accession_1', 'Regio_crist_1', 'Segments_TM_1', 'Pfam1'], axis=1)
acc_membrana = list(df_pdb['Accession'])
unip1 = list( df_hetero_comp_2['Accession_2'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)
        
df_pdb_com_het_2 = pandas.DataFrame(df_pdb, index = indexu)
df_pdb_com_het_2.index= range(len(df_pdb_com_het_2))

acc_membrana = list(df_pdb_com_het_2['PDB'])
unip1 = list( df_hetero_comp_2['PDB'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)

df_pdb_com_het_2 = pandas.DataFrame(df_pdb_com_het_2, index = indexu)
df_pdb_com_het_2.index= range(len(df_pdb_com_het_2))

df_pdb_com_het_2 = df_pdb_com_het_2.drop(['ID', 'Gene', 'Organism', 'Method', 'Angstrom'], axis=1)
df_pdb_com_het_2.columns=['Accession', 'PDB', 'Chains', 'Regio_crist'] 

df_hetero_comp_22 = df_hetero_comp_2.merge(df_pdb_com_het_2, on="PDB", how="left")
df_hetero_comp_22 = df_hetero_comp_22.drop(['Accession', 'Regio_crist'], axis=1)

df_hetero_comp_22.columns = ['PDB', 'Accession_2', 'Regio_crist_2', 'Segments_TM_2', 'Pfam2', 'Chains_2']

df_hetero_comp_def = df_hetero_comp_11.merge(df_hetero_comp_22, on="PDB", how="left")

df_hetero_comp_def.to_csv("pdb_pisa_hetero_mem_compl_def_1.csv")   

#HOMOS
df_homo = pd.DataFrame({"Region":[], "Prov":[]})
regioH = list(df_pdbPisa_homo_mem_def['Regio_crist'])
tmH = list(df_pdbPisa_homo_mem_def['Segments_TM'])
index_homo_1 = ""
ind_hom = []
compt = -1
for i, element in enumerate(regioH):
    r = regioH[i]
    tmi = tmH[i]
    resultats={range(int(r[0]),int(r[1])): "YES"}
    for j,element in enumerate(tmi):
        tmx =tmi[j].split("..")
        for k, element in enumerate(tmx):
            for rango, resultado in resultats.items():
                if int(tmx[k]) in rango:
                    break
                else:
                    resultado = "NO"
            if resultado == "YES":
                index_homo_1 = (i)
                ind_hom.append(i)
                compt += 1
                if ind_hom[compt] != ind_hom[compt-1]:
                    pdb_ap_uni = [index_homo_1, resultado]
                    pdb_series = pd.Series(pdb_ap_uni, index = df_homo.columns)
                    df_homo = df_homo.append(pdb_series, ignore_index=True)
                if compt == 0:
                    pdb_ap_uni = [index_homo_1, resultado]
                    pdb_series = pd.Series(pdb_ap_uni, index = df_homo.columns)
                    df_homo = df_homo.append(pdb_series, ignore_index=True)

ind_hom = list(df_homo['Region'])
    
df_pdbPisa_homo_mem_def_1 = pandas.DataFrame(df_pdbPisa_homo_mem_def, index = ind_hom)
df_pdbPisa_homo_mem_def_1.index= range(len(df_pdbPisa_homo_mem_def_1))

acc_membrana = list(df_pdb['PDB'])
unip1 = list( df_pdbPisa_homo_mem_def_1['PDB'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)

df_pdb_com = pandas.DataFrame(df_pdb, index = indexu)
df_pdb_com.index= range(len(df_pdb_com))

acc_membrana = list(df_pdb_com['Accession'])
unip1 = list( df_pdbPisa_homo_mem_def_1['Accession'])

unip1m =[]
indexu = []
for i, element in enumerate(acc_membrana):
    print(f"> {(i+1)/len(acc_membrana)} %")
    if element in unip1:
        indexu.append(i)
        unip1m.append(element)
        print(element)
df_pdb_com = pandas.DataFrame(df_pdb, index = indexu)
df_pdb_com.index= range(len(df_pdb_com))

df_pdb_com = df_pdb.drop(['ID', 'Gene', 'Organism', 'Method', 'Angstrom'], axis=1)
df_pdb_com.columns=['Accession', 'PDB', 'Chains', 'Regio_crist'] 

df_pdbPisa_homo_mem_def_2 = df_pdbPisa_homo_mem_def_1.merge(df_pdb_com, on="PDB", how="left")
df_pdbPisa_homo_mem_def_2.index= range(len(df_pdbPisa_homo_mem_def_2))
df_pdbPisa_homo_mem_def_3 = df_pdbPisa_homo_mem_def_2.drop([518, 520, 237],axis=0)
df_pdbPisa_homo_mem_def_3.index= range(len(df_pdbPisa_homo_mem_def_3))

df_pdbPisa_homo_mem_def_3 = df_pdbPisa_homo_mem_def_3.drop(['Accession_y', 'Regio_crist_y'], axis=1)
df_pdbPisa_homo_mem_def_3.columns =[ 'PDB', 'Accession', 'Regio_crist', 'Segments_TM', 'Pfam', 'Chains']
df_pdbPisa_homo_mem_def_3.to_csv("pdb_pisa_homo_mem_compl_def_1.csv")   

