#!/usr/bin/python3
###############################################################################
# IMPORTS
###############################################################################
import os
import sys
#from gen_distribution import *
from pymol import cmd
import numpy as np
import MDAnalysis as mda
import MDAnalysis.analysis.distances
import itertools  # Combinations without repetition
import glob
import collections
from mysql.connector import connect
import requests
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
import re
import pandas as pd
import sqlalchemy
from time import sleep
from termcolor import colored
import pymol


###############################################################################
# FUNCTIONS
###############################################################################

def get_files(path, ext):
    """Obtain a list that contain the name of all pdb file in a directory"""
    l_files = []
    folder = glob.glob(path + "*" + ext)
    for path_filename in folder:
        path_filename = path_filename.split(
            "\\"
        )  # ['', 'home', 'adrian', 'Escritorio', 'TFG', 'dimerInteractions', '2Z73_B_OPSD_TODPA.pse']
        filename = path_filename[-1]
        filename = filename.split(".")  # 4WW3_A_OPSD_TODPA.pse
        filename = filename[0]  # 4WW3_A_OPSD_TODPA
        l_files.append(filename)
    return sorted(l_files)


def get_z_values(files):
    m_pdbs = []
    for f in files:
        name = f.split("/")
        name = name[-1]
        u = mda.Universe(f)
        prot_atom = u.select_atoms("protein")
        coords = prot_atom.positions
        print(name, u, abs(coords[:, 2].max()) + abs(coords[:, 2].min()))
        if abs(coords[:, 2].max()) + abs(coords[:, 2].min()) > 100:
            m_pdbs.append(f)
    return m_pdbs


def open_file(path, filename):
    """Read a file and save it on a variable"""
    f_in = open(path + filename, "r")
    f_r = f_in.readlines()  # All info here
    f_in.close()
    return f_r


def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
        print("> Created directory: " + path)


def load_pymol(path_pdb, prot, ext):
    # Open reference protein
    cmd.load(path_pdb + prot + ext, quiet=0)
    print("> Loading " + prot + ext)
    sleep(0.5)


def start_pymol():
    # Open pymol
    print("Open PyMol!")
    pymol.finish_launching(["pymol", "-qc"])



def quit_pymol():
    # Close pymol
    print("> Closing PyMol!")
    pymol.cmd.quit()


def com_pymol(prot, chain):
    # Get center of mass (both chains, angle of reference)
    ref_center = cmd.centerofmass(
        prot + """ and not resname UNX and chain """ + chain
    )  # List of 3 floats
    print("> Center of mass on " + str(ref_center))
    sleep(0.5)
    return ref_center


def pseudoatom_pymol(name, coordinates, reference, indicator):
    # Create pseudoatoms of reference
    if reference == "TRUE":
        cmd.do(
            """pseudoatom """
            + name
            + """_refcenter, pos ="""
            + str(coordinates)
            + """ """
        )
        sleep(0.5)
    else:
        cmd.do(
            """pseudoatom """
            + name
            + """_center"""
            + indicator
            + """, pos ="""
            + str(coordinates)
            + """ """
        )
        sleep(0.5)
    print("> Pseudoatom created on " + str(coordinates))


def perform_align(prot_a, chain_a, prot_b, chain_b):
    # Superposition of two chain of a dimer
    cmd.do(f"select struc, {prot_b} and chain {chain_b}")
    cmd.do(f"select ref, {prot_a}_ref and chain {chain_a}")
    super_rmsd = cmd.super("struc", "ref")
    sleep(0.5)
    align_rmsd = cmd.align("struc", "ref")
    sleep(0.5)
    if super_rmsd[0] > align_rmsd[0]:
        print("> Chains aligned!")
    else:
        super_rmsd = cmd.super("struc", "ref")
        sleep(0.5)
        print("> Chains superposed!")


def color_pymol(color, name):
    # Color these pseudoatoms
    cmd.color(color, name)


def get_vector(coord_a, coord_b):
    coord_a = np.array(coord_a)
    coord_b = np.array(coord_b)
    vector = (coord_b - coord_a) / np.linalg.norm(coord_b - coord_a)
    return vector


def get_angle_pymol(path_pdb, prot_ref, prot, chain_ref, chains, vector_ref, indicator):
    # Get the angle of three points
    name = prot + "_" + str(chains[0] + chains[1])
    cmd.set_name(prot, name)
    prot = prot + "_" + str(chains[0] + chains[1])
    name = prot.split("_")
    perform_align(prot_ref, chain_ref, prot, chains[0])
    center_1 = com_pymol(prot, chains[0])
    pseudoatom_pymol(prot + "_" + chains[0], center_1, "FALSE", indicator)
    sleep(1)
    color_pymol(2, prot + "_" + chains[0] + """_center""" + indicator)
    cmd.do(f'label {prot}_{chains[0]}, "{name[0]}_{name[1]}_{name[2]}"')
    center_2 = com_pymol(prot, chains[1])
    pseudoatom_pymol(prot + "_" + chains[1], center_2, "FALSE", indicator)
    sleep(1)
    color_pymol(2, prot + "_" + chains[1] + """_center""" + indicator)
    cmd.do(f'label {prot}_{chains[1]}, "{name[0]}_{name[1]}_{name[2]}" ')
    vector_nref = get_vector(center_1, center_2)
    angle = np.arccos(np.clip(np.dot(vector_ref, vector_nref), -1.0, 1.0))
    angle = np.degrees(angle)
    sleep(0.5)
    return (angle, center_1, center_2)


###############################################################################
# PATHS AND DICTIONARIES
###############################################################################
path = os.path.dirname(os.path.abspath("__file__"))
sys.path.insert(0, path)
path_pse = path + "\\PSE\\"
path_opm = path + "\\OPM\\"
path_pdb = path + "\\PDB\\"
path_misc = path + "\\Misc\\"
path_dimer_lz = path + "\\PDB\\dimers_origin\\"
path_dimer = path + "\\PDB\\dimers_nlz\\"
path_dimer_end = path + "\\PDB\\dimers\\"
path_removed = path + "\\PDB\\dimers_removed\\"
d_change = {"A": "Q", "B": "R", "C": "S", "D": "T", "E": "U", "F": "V"}

uni = pd.read_csv("G:/Estadistica4/TFG/Python/pdb_entries2.csv", delimiter=',')

###############################################################################
# FUNCTIONS
###############################################################################
def get_coord_lz(path_file):
    coord = open_file(path_file, "fusion_residues.txt")
    d_coord = {"default": "-1000-0+900-2000"}  # default is to remove -1000-0+900-2000
    del coord[0]
    for c in coord:
        line = c.split(" ")
        d_coord[line[0]] = line[1][:-1]
    return d_coord


def rem_lz(lz, lz_info, path_dimer_lz, path_dimer, cutoff_lz):
    codes = lz_info.keys()
    cmd.load(path_dimer_lz + lz + ".pdb", quiet=0)
    name = lz.split("_")
    if name[0] in codes:
        cmd.do(f"select resi {lz_info[name[0]]} and not het")
        cmd.do("remove sele")
    # Normal case
    else:
        cmd.do(
            f'select resi {lz_info["default"]} and not het'
        )  # default is to remove -1000-0+900-2000
        cmd.do("remove sele")
    sleep(1)
    cmd.do(
        "select  not byres (all and not het) around " + cutoff_lz + " + all and not het"
    )
    cmd.do("remove sele")
    cmd.save(path_dimer + lz + ".pdb")
    sleep(1)


def rem_chains(path):
    dic = {}
    lines = open_file(path, "pdb_mappings.txt")
    for line in lines:
        line = line[:-1]
        line = line.split("  ")
        for i in line:
            if i != line[0]:
                if line[0] in dic:
                    dic[line[0]] = f"{dic[line[0]]}+{i[0]}"
                else:
                    dic[line[0]] = i[0]
    return dic


def get_opm(path_opm, pdb):
    link = "https://opm-assets.storage.googleapis.com/pdb/"
    urlretrieve(link + pdb + ".pdb", path_opm + pdb + ".pdb")


def reference_dict(a_pdb, b_pdb, d_ref):
    if a_pdb != b_pdb:
        if not a_pdb in d_ref.keys():  # NO
            if b_pdb in d_ref.keys():  # YES, eliminated
                d_ref[b_pdb] = f"{d_ref[b_pdb]}, {b_pdb}"  # Add himself
                d_ref[a_pdb] = d_ref[b_pdb]  # Append all the list to the not removed
                del d_ref[b_pdb]  # Remove eliminated
            else:  # NO, eliminated (first time on dict)
                d_ref[a_pdb] = b_pdb
        else:  # YES
            if not b_pdb in d_ref[a_pdb]:  # No repeat
                d_ref[a_pdb] = f"{d_ref[a_pdb]}, {b_pdb}"


###############################################################################
# GENERATE ALL PDBS
###############################################################################
cutoff_rmsd = 10 
cutoff_lz = str(8) #no s'utilitza
min_dist = str(10)
option_generate = False
option_nlz = False
option_dist = True #False
option_super = True #False
option_check = False

if __name__ == "__main__":
    print('ENTRA')
    create_dir(path_pdb)
    create_dir(path_dimer_lz)
    create_dir(path_dimer_end)
    create_dir(path_misc)
    create_dir(path_dimer)
    create_dir(path_opm)
    create_dir(path_removed)

    # Get the all unique code present on pse
    l_psefiles = get_files(path_pse, ".pse")
    l_pdbs_codes = get_files(path_dimer_end, ".pdb")
    l_pdbs = get_files(path_dimer_lz, ".pdb")
    # l_pdbs = get_files(path_pse, ".pse")
    l_pdb_code = []
    for pdb in l_pdbs:
        pdb = pdb.split("_")
        pdb = pdb[0]
        if not pdb in l_pdb_code:
            l_pdb_code.append(pdb)
    # Get the last id:
    print('FET')
    if l_pdbs_codes != []:
        print(l_pdbs_codes)
        for pdb in l_pdbs_codes:
            pdb = pdb.split("_")
            print(pdb)
            last_id = pdb[2]
    else:
        print('OK!')
        last_id = 1
        
    start_pymol()
    cmd.do("set retain_order, 1")
    cmd.do("set pdb_use_ter_records, 1")  # INSERT TER LINE
    cmd.do("set ignore_pdb_segi, 1")
    sleep(1)

    # l_psefiles = ['3ODU_A_CXCR4_HUMAN', '3ODU_B_CXCR4_HUMAN', '3UON_A_ACM2_HUMAN']
    dic_chains = rem_chains(path_misc)  # Get info about real chains of the dimer
    if option_generate == True:
        j = 1
        for psefile in l_psefiles:
            pse = psefile.split("_")
            pse = pse[0]
            if not pse in l_pdb_code:
                print(f"> {j *100 / len(l_psefiles)} %")
                file_okay = glob.glob(f"{path_dimer_lz}{psefile}*.pdb")
                if file_okay == []:
                    print(f"> Getting dimers from {psefile}")
                    load_pymol(path_pse, psefile, ".pse")
                    name = psefile.split("_")
                    if name[0] in dic_chains:
                        cmd.do(
                            f"select * and not chain {dic_chains[name[0]]} and not het"
                        )
                        cmd.remove("sele")
                    l_objects = cmd.get_object_list("(all)")
                    for a, b in itertools.combinations(
                        l_objects, 2
                    ):  # Obtain all dimers NO FILTER!!
                        chain_a = cmd.get_chains(a)
                        chain_b = cmd.get_chains(b)
                        if chain_a[0] == chain_b[0]:
                            cmd.do(
                                f'alter {b}, chain= "{d_change[chain_b[0]]}"'
                            )  # A --> Q'
                            cmd.save(
                                f"{path_dimer_lz}{psefile}_{a}_{b}_{chain_a[0]}{d_change[chain_b[0]]}.pdb",
                                f"{a} + {b}",
                            )
                            cmd.do(f'alter {b}, chain= "{chain_b[0]}"')
                        else:
                            cmd.save(
                                f"{path_dimer_lz}{psefile}_{a}_{b}_{chain_a[0]}{chain_b[0]}.pdb",
                                f"{a} + {b}",
                            )
                        sleep(1)
                    cmd.reinitialize()
                    sleep(1)
            j += 1

    ################################################################################
    ## REMOVE LISOZIM(FILTER I)
    #################################################################################
    # start_pymol()
    lz_files = get_files(path_dimer_lz, ".pdb")
    dimer_files = get_files(path_dimer, ".pdb")
    # lz_files = ['4UG2_A_AA2AR_HUMAN_4UG2A_symB03-10000_AB', '4UG2_B_AA2AR_HUMAN_4UG2B_symA03000000_BA']
    lz_info = get_coord_lz(path_misc)  # Get info about res related with fusion protein
    if option_nlz == True:
        j = 1
        for lz in lz_files:
            print(f"> {j *100 / len(lz_files)} %")
            if not os.path.exists(f"{path_dimer}{lz}.pdb"):
                print(f"> Removing fusion protein from {lz}")
                rem_lz(
                    lz, lz_info, path_dimer_lz, path_dimer, cutoff_lz
                )  # Open pdb and remove lisozim
                cmd.reinitialize()
            j += 1
            sleep(0.5)
    # quit_pymol()
    #############################################################################
    # MINIMUM DISTANCE (FILTER II)
    #############################################################################
    nlz_files = get_files(path_dimer, ".pdb")
    # nlz_files = ['5F8U_A_ADRB1_MELGA_5F8UA_symB00000100']
    if option_dist == True:
        j = 1
        total = len(nlz_files)
        for nlz in nlz_files:
            print(f"> {j *100 / total} %")
            print(f"> {nlz}")
            try:
                u = mda.Universe(f"{path_dimer}{nlz}.pdb")
            except:
                print(colored("Error open!", "red"))
                os.remove(f"{path_dimer}{nlz}.pdb")
                continue
            name = nlz.split("_")
            print(name)
            g1 = u.select_atoms(f"name CA and segid {name[5][0]}")
            g2 = u.select_atoms(
                f"name CA and segid {name[5][1]} and around {min_dist} protein and name CA and segid {name[1]}"
            )
            print(g1,g2)
            dist = mda.analysis.distances.distance_array(g1.positions, g2.positions)
            #sleep(5)
            print(dist)
            if dist.size > 0:
                mindist = np.amin(dist)
            else:
                fileList = glob.glob(f'{path_dimer}*.pdb', recursive=True)
                filepdb =  f"{path_dimer}{nlz}.pdb" 
                if  filepdb    in fileList:
                    try:
                        os.remove(filepdb)
                    except:
                        print("Error while deleting file")
                    #if os.path.exists(f"{path_dimer}{nlz}.pdb"):
                
                    # print(f"> Removing {nlz}")
                    # try:
                    #     os.remove(f"{path_dimer}{nlz}.pdb")
                    # except:
                    #     print(f'error {path_dimer}{nlz}.pdb')
            j += 1

    ###############################################################################
    # SUPERPOSITION WITH TOPOLOGY (FILTER III)
    ###############################################################################
    nlz_files = get_files(path_dimer, ".pdb")
    del_filtr3 = []
    # ORIENTATION RIGHT
    # start_pymol()
    # cmd.do('set retain_order, 0') #This setting affects the command super.
    if option_super == True:
        for nlz in nlz_files:
            opm = nlz.split("_")
            opm = opm[0].lower()
            data = requests.get(
                "https://opm-assets.storage.googleapis.com/pdb/" + opm
            ).text
            soup = BeautifulSoup(data, "html.parser")  # Obtain a soup of info.
            if not os.path.exists(f"{path_opm}{opm}.pdb"):
                get_opm(path_opm, opm)
                print(f"> Loading dimer {nlz} for orientation")
                load_pymol(path_dimer, nlz, ".pdb")
                print(f"> Loading opm structure {opm}")
                load_pymol(path_opm, opm, ".pdb")
                try:
                    super_rmsd = cmd.super(nlz, opm)
                    sleep(0.5)
                    align_rmsd = cmd.align(nlz, opm)
                    sleep(0.5)
                except:
                    continue
                if super_rmsd[0] > align_rmsd[0]:
                    print("> Chains aligned!")
                else:
                    super_rmsd = cmd.super(nlz, opm)
                    sleep(0.5)
                    print("> Chains superposed!")
                # cmd.do('set retain_order, 1')
                cmd.do(f"save {path_dimer}{nlz}.pdb, {nlz}")
                # cmd.do('set retain_order, 0')
                cmd.reinitialize()

        ##GET DICCIONARY OF RESOLUTIONS OF EACH STRUCTURE (CODI PDB)
#        try:
#           engine = sqlalchemy.create_engine(
        #         f"mysql://lmcdbuser:{os.getenv('LMCDBUSER_PASS')}@alf03.uab.cat/lmcdb"
        #     )
        #     print(engine)
        #     # engine = sqlalchemy.create_engine(f"mysql+mysqlconnector://adrian:D1m3rB0w!@localhost:3306/lmcdb")
        # except:
        #     print(colored("could not connect", "red"))
        #     sys.exit(0)

        # sql_query = """SELECT pdbid, resolution FROM pdb
        # WHERE resolution < 90 and (domain REGEXP '7tm.' or domain='Frizzled') and expmet='X-RAY DIFFRACTION';"""
        # data = pd.read_sql(sql_query, engine)
        # l_pdbid = list(data.pdbid)
        # l_res = list(data.resolution)
    #Agafar informació taula dades uniprot
        l_pdbid = list(uni['PDB'])
        l_res = list(uni['Angstrom'])
        d_res = {}
        for i, pdb in enumerate(l_pdbid):
            d_res[pdb] = l_res[i]

        # ##RMSD ANALYZE
        # sleep(1)
        # remove = open(path_misc + "removed.txt", "w")
        # remove.writelines(f"Reference Removed RMSD \n")
        d_ref = {}
        for a, b in itertools.combinations(nlz_files, 2):
            a_pdb = a.split("_")
            a_pdb = a_pdb[0]
            b_pdb = b.split("_")
            b_pdb = b_pdb[0]
            if (
                a[7:12] == b[7:12]
                and os.path.exists(f"{path_dimer}{a}.pdb")
                and os.path.exists(f"{path_dimer}{b}.pdb")
            ):
                load_pymol(path_dimer, a, ".pdb")
                load_pymol(path_dimer, b, ".pdb")
                cmd.do("set retain_order, 0")
                chains_a = cmd.get_chains(a)
                chains_b = cmd.get_chains(b)
                cmd.do("sort")
                # rmsd = cmd.do(f'super {a}, {b}')
                rmsd_a = cmd.align(f"{b} and not het", f"{a} and not het")
                rmsd_s = cmd.super(b, a)
                if round(rmsd_a[0]) < round(rmsd_s[0]):
                    rmsd = rmsd_a
                else:
                    rmsd = rmsd_s
                # print (rmsd)
                if (
                    round(rmsd[0]) <= cutoff_rmsd
                ):  # SELECTED LOOKING DIFERENTS STRUCTURES
                    if (
                        d_res[a_pdb] < d_res[b_pdb] or d_res[a_pdb] == d_res[b_pdb]
                    ):  # REMOVE THE DIMER WITH MAJOR RESOLUTION
                        print(colored(f"> Removing {b}", "red"))
                        # remove.writelines(f"{a} {b} {rmsd} \n")
                        reference_dict(a_pdb, b_pdb, d_ref)
                        try:
                            os.remove(f"{path_dimer}{b}.pdb")
                        except:
                            del_filtr3.append(f"{b}.pdb")
                        try:
                            nlz_files.remove(b)
                        except:
                            continue
                    else:
                        print(colored(f"> Removing {a}", "red"))
                        # remove.writelines(f"{a} {b} {rmsd} \n")
                        reference_dict(b_pdb, a_pdb, d_ref)
                        try:
                            os.remove(f"{path_dimer}{a}.pdb")
                        except:
                            del_filtr3.append(f"{a}.pdb")
                        try:
                            nlz_files.remove(a)
                        except:
                            continue
                else:  # NOW DETECT IF WE SORT THE CHAINS TO INVERSE IT IS THE SAME DIMER
                    # cmd.do('set retain_order, 1')
                    cmd.do(f'alter {b} and chain {chains_b[0]}, chain = "temp"')
                    cmd.do(
                        f'alter {b} and chain {chains_b[1]}, chain = "{chains_b[0]}"'
                    )
                    cmd.do(f'alter {b} and chain temp, chain = "{chains_b[1]}"')
                    cmd.do("sort")
                    rmsd_a = cmd.align(f"{b} and not het", f"{a} and not het")
                    rmsd_s = cmd.super(b, a)
                    if round(rmsd_a[0]) < round(rmsd_s[0]):
                        rmsd = rmsd_a
                    else:
                        rmsd = rmsd_s
                    # print (rmsd)
                    if (
                        round(rmsd[0]) <= cutoff_rmsd
                    ):  # IF IT IS THE SAME REMOVE THE DIMER
                        if (
                            d_res[a[0:4]] < d_res[b[0:4]]
                            or d_res[a[0:4]] == d_res[b[0:4]]
                        ):  # REMOVE THE DIMER WITH MAJOR RESOLUTION
                            print(colored(f"> Removing {b}", "red"))
                            reference_dict(a_pdb, b_pdb, d_ref)
                            try:
                                os.remove(f"{path_dimer}{b}.pdb")
                            except:
                                del_filtr3.append(f"{b}.pdb")
                            # remove.writelines(f"{a} {b} {rmsd} \n")
                            try:
                                nlz_files.remove(b)
                            except:
                                continue
                        else:
                            print(colored(f"> Removing {a}", "red"))
                            reference_dict(b_pdb, a_pdb, d_ref)
                            try:
                                os.remove(f"{path_dimer}{a}.pdb")
                            except:
                                del_filtr3.append(f"{a}.pdb")
                            # remove.writelines(f"{a} {b} {rmsd} \n")
                            try:
                                nlz_files.remove(a)
                            except:
                                continue
            else:
                continue
            cmd.reinitialize()
        # REFERENCE PDB CODES TO REMOVED ONES
        reference = open(path_misc + "references.txt", "w")
        reference.writelines(f"Reference PDB_codes \n")
        for key in d_ref.keys():
            reference.writelines(f"{key} {d_ref[key]}\n")
        reference.close()
    # remove.close()

    ###############################################################################
    # RENAME PDBS
    ###############################################################################
    if option_check == True:
        last_id = int(last_id)
        for code in l_pdb_code:
            dimers_id = get_files(path_dimer_end + code + "*", ".pdb")
            dimers = get_files(path_dimer + code + "*", ".pdb")
            i = 0
            possible = False
            possible_dimer = ""
            if dimers_id == [] and not dimers == []:
                for d in dimers:
                    last_id = last_id + 1
                    name = d.split("_")
                    name = name[0] + "_" + name[2]
                    print(colored((code, d, last_id, name), "green"))
                    os.system(
                        f"cp {path_dimer}{d}.pdb {path_dimer_end}{name}_{str(last_id)}.pdb"
                    )
            if dimers == [] and not dimers_id == []:
                for d in dimers_id:
                    name = d.split("_")
                    name = name[0] + "_" + name[2]
                    print(colored((code, d, last_id, name), "red"))
                    os.system(f"mv {path_dimer_end}{d}.pdb {path_removed}{d}.pdb")
                continue
            for d_id in dimers_id:
                load_pymol(path_dimer_end, d_id, ".pdb")
                for i, d in enumerate(dimers):
                    load_pymol(path_dimer, d, ".pdb")
                    rmsd_s = cmd.super(d, d_id)
                    if int(round(rmsd_s[0])) == 0:
                        print(f"> Dimer {d} exists!")
                        continue
                    else:
                        print(f"> Dimer {d} NOT exists!")
                        if possible == True and possible_dimer == d:
                            last_id = last_id + 1
                            name = d.split("_")
                            name = name[0] + "_" + name[2]
                            print(colored((code, d, last_id, name), "green"))
                            os.system(
                                f"cp {path_dimer}{d}.pdb {path_dimer_end}{name}_{str(last_id)}.pdb"
                            )
                        possible = True
                        possible_dimer = d
                    i += 1
                cmd.reinitialize()
    quit_pymol()
##############################################################################
# END
f = open("del_filtre_gen_dimers.txt", "a")

for i in del_filtr3:
    f.write(i + "\n")
    

f.close()
