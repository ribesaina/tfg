#!/usr/bin/python3

##### IMPORT
# os package gives to python functions to manage files or directories like we are working in a terminal using bash.
import os

# Bio.PDB is a package from biopython (super library with a lot of open source libraries to be used free) that gives functions to manage pdbs
from Bio.PDB import *

##### MAIN PROGRAM
# 1. Create the directory that will contain all the PDB files. The next statements creates the directory from the path that you execute the script.
path_pdb = "./PDB/"
if not os.path.exists(path_pdb):  # Check if the directory exists
    os.makedirs(path_pdb)  # Creates the directory (bash: mkdir command)
    print("> Created directory: " + path_pdb)  # Check
else:
    print("> Directory found!")

# 2. Get the list of PDB ids
pdb_list = ["4DKL"]

# 3. Get the PDB files from the server using the Bio.PDB package
pdb_server = PDBList()  # Performs the connection between the user & server
pdb_fails = []  # Empty list that will contain possible fail downloads.

for i, pdb in enumerate(pdb_list):  # Passthrough the list that contains the PDB codes.
    print(f"> {((i+1)/len(pdb_list))*100} % ({pdb})")  # Get the processing
    try:
        pdb_server.retrieve_pdb_file(
            pdb, file_format="pdb", pdir=path_pdb
        )  # Get the file. Especified the file_format (ent) & directory path
    except:  # If it fails get the list of pdbs.
        pdb_fails.append(pdb)

print("> Done!")
