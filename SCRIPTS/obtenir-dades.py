# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 18:53:35 2021

@author: Aina Ribes
"""

## IMPORTS 
from io import open
import pandas as pd

# Open file
archivo_texto=open("6108-uniprot.txt", "r")
dades=archivo_texto.readlines()
archivo_texto.close()

# Create empty dataframes ({"Name column":[]})
df_pdb = pd.DataFrame({"ID":[], "Accession":[], "Gene":[], "Organism": [], "PDB":[], "Method":[], "Angstrom":[], "Chains":[], "Coords":[]})
df_pfam = pd.DataFrame({"ID":[], "Accession":[], "Gene":[], "Organism": [], "Pfam":[], "Name":[]})

# Create empty lists that will be included into the both dataframes 
l_acc = []

# Parse of the file, clean of the data and extract the information to the dataframes
for i, element in enumerate(dades):
    print(f"> {(i+1)/len(dades)} %")
    if element.startswith('ID'): #ID   PHAG1_RAT               Reviewed;         424 AA.
        l = element.split(" ")
        l = list(filter(None,l)) #['ID', 'PHAG1_RAT', 'Reviewed;', '424', 'AA.\n']
        l_acc.append(l[1])
    if element.startswith('AC'): #AC   Q9JM80;
        l = element.split(" ")
        l = list(filter(None,l))
        l = l[1].replace("\n", "").split(";")
        l = list(filter(None,l)) #['Q9JM80']
        l = ",".join(l)
        l_acc.append(l)
    if element.startswith('GN'): #GN   Name=Pag1; Synonyms=Cbp, Pag;
        if "Name=" in element: #Some entries have 3 entries Name, ORFNames, OrderedLocusNames, Synonyms and Visa
            l = element.split(" ")
            l = list(filter(None,l)) #['GN', 'Name=Pag1;', 'Synonyms=Cbp,', 'Pag;\n']
            l = l[1].split("=")
            l = l[1].replace(";", "")
            l_acc.append(l)
    if element.startswith('OS'): #OS   Rattus norvegicus (Rat).
        l = element.split(" ")
        l = list(filter(None,l)) #['OS', 'Rattus', 'norvegicus', '(Rat).\n']
        try: # Some entries have the Organism name into two lines
            l_acc.append(l[1] + " " + l[2])
        except:
            pass
    if element.startswith('DR   PDB;'): #DR   PDB; 5H23; X-ray; 2.20 A; A/B=106-261.
        l = element.split(";")
        if (("X-ray" in l[2]) or ("EM" in l[2])): #['DR   PDB', ' 5H23', ' X-ray', ' 2.20 A', ' A/B=106-261.\n']
            pdb = l[1].replace(" ","")
            met = l[2].replace(" ","")
            ang = l[3].replace(" ","").replace("A", "")
            ch_coor = l[4].split("=") #[' A/B', '106-261.\n']
            pdb_ap = list(l_acc)
            if len(pdb_ap) == 3: # Some entries don't have gene 
                pdb_ap.insert(2, "-")
            if len(pdb_ap) >= 5: # Some entries have 2 or more AC rows 
                pdb_ap = pdb_ap[:2] + pdb_ap[-2:]
            pdb_ap.append(pdb)
            pdb_ap.append(met)
            pdb_ap.append(ang)
            try:
                ch = ch_coor[0].replace(" ", "")
                coord = ch_coor[1].replace(".\n", "")
                pdb_ap.append(ch)
                pdb_ap.append(coord)
            except: #['DR   PDB', ' 1GQ5', ' X-ray', ' 2.20 A', ' -.\n']
                pdb_ap.append("-")
                pdb_ap.append("-")
            pdb_series = pd.Series(pdb_ap, index = df_pdb.columns) #List to Series (Dataframes need it)
            df_pdb = df_pdb.append(pdb_series, ignore_index=True) #Append the serie as a row
    if element.startswith('DR   Pfam;'): #DR   Pfam; PF15347; PAG; 1.
        l = element.split(";") #['DR   Pfam', ' PF15347', ' PAG', ' 1.\n']
        pfam = l[1].replace(" ","")
        name = l[2].replace(" ", "")
        pfam_ap = list(l_acc)
        if len(pfam_ap) == 3: # Some entries don't have gene 
            pfam_ap.insert(2, "-")
        if len(pfam_ap) >= 5: # Some entries have 2 or more AC rows 
            pfam_ap = pfam_ap[:2] + pfam_ap[-2:]
        pfam_ap.append(pfam)
        pfam_ap.append(name)
        pfam_series = pd.Series(pfam_ap, index = df_pfam.columns) #List to Series (Dataframes need it)
        df_pfam = df_pfam.append(pfam_series, ignore_index=True) #Append the serie as a row
    if element.startswith('//'):
        l_acc = []

# Print dataframes or export file as excel
df_pdb.to_csv("pdb_entries2.csv")
df_pfam.to_csv("pfam_entries2.csv")


