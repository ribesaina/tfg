# TFG

## Generation of database

**obtenir-dades.py** (95 lines)

- Read the downloaded Uniprot file with all the data
- Extract the necessary information

**pdb_extractor.py** (35 lines)

- Download all PDB files of interest

**check_membrane.py** (168 lines)

- Check that all proteins are membrane

**compare_ranges.py** (213 lines)

- Compares that the range of crystallized regions of each protein includes part of the transmembrane segments

## Generation of all possible dimers

**gen_oligos.py** (318 lines, 5 functions)


- Takes the list of protein XRAY structures
- Generates the symmetry mates using Pymol
- Does a first filtering of dimers:
  - dimers are in the same plane based on OPM (https://opm.phar.umich.edu/) positioning (distance between centers  in the Z-directoin > 10 A.
  - maximum distance between centers > 2*radi+10%
  - angle (normal to the membrane) between dimers < 90º
- The output is a collection of .pse files 

**gen_dimers.py** (664 lines, 18 functions)

Generate the dimers using the PyMol sessions (.pse format) generated by gen_oligos.py


- Opens the list of sessions generated by gen_oligos.py (extension .pse)
- Generates all possible pairs of monomers (dimers) without filtering (503 dimers, May 10, 2018) using only the chains of the dimers (remove chains like peptides present in the session)  
- Does three steps of filtering on all generated dimers:
  - 1) Remove the fusion-protein according to specific coordinates in each structure and remove atoms around 8 A of the dimer 
  - 2) Remove the structures with the separation between the center-of-mass of the two protomers < 10 A(only select CA)
  - 3) Remove dimers of the same protein with the same topology (rmsd of CA <=3 nm2). Keep the structure with the best resolution.
- Use `pdb_mappings.txt, fusion_residues.txt` and `list_pdb_resol.txt` files.
- Generate too gooddimers.txt, topology.txt and removed.txt like control files.


## Analysis of the simulations

**get_interactions.py** (1075 lines, 27 functions)

Get the interactions of each .pdb or .gro file used in simulations. 


- For each dimer, generate an output text file with the list of each interaction between the two protomers. Example:
      `A_O_ALA_59_ICL1 B_SG_CYS_341_CT 4.01 -17.88199997 PH` 
       a_b_c___d__e    f_g__h___i___j    k    l          m
  - a, f: Chain.
  - b, g: Atom type
  - c, h: Residue name
  - d, i: Residue Id
  - e, j:  Localization
  - k: Distance (Å).
  - l: Height on the Z-axis (average of the z coordinates of each atom).
  - m: Type of interaction (Hydrophobic, Polar, Charged or Aromatic) 
  
- After the generation of the files the script generates files with the counting of this interactions by zone (count_interactions.py).



