# -*- coding: utf-8 -*-
"""
Created on Wed May  5 08:51:45 2021

@author: Aina Ribes
"""
import pandas
import pandas as pd

acc_membrana = list(df_pdb['Accession'])
unip1 = list( df_pdbPisa_hetero['UNIPROT 1'])

unip1m =[]
indexu = []
for i, element in enumerate(unip1):
    print(f"> {(i+1)/len(unip1)} %")
    if element in acc_membrana:
        indexu.append(i)
        unip1m.append(element)
        print(element)

df_pdbPisa_hetero_1 = pandas.DataFrame(df_pdbPisa_hetero, index = indexu)
df_pdbPisa_hetero_1.index= range(len(df_pdbPisa_hetero_1))
unip2 = list( df_pdbPisa_hetero_1['UNIPROT 2'])

unipm =[]
indexu2 = []
for i, element in enumerate(unip2):
    print(f"> {(i+1)/len(unip2)} %")
    if element in acc_membrana:
        indexu2.append(i)
        unipm.append(element)
        print(element)
    if element == "":
        indexu2.append(i)
        unipm.append(element)
        print(element)

df_pdbPisa_hetero_mem = pandas.DataFrame(df_pdbPisa_hetero_1, index = indexu2)

df_pdbPisa_hetero_mem.to_csv("pdb_pisa_hetero_mem.csv")


unip1h = list( df_pdbPisa_homo['UNIPROT'])
unip1mh =[]
indexuh = []
for i, element in enumerate(unip1h):
    print(f"> {(i+1)/len(unip1h)} %")
    if element in acc_membrana:
        indexuh.append(i)
        unip1mh.append(element)
        print(element)

df_pdbPisa_homo_mem = pandas.DataFrame(df_pdbPisa_homo, index = indexuh)

df_pdbPisa_homo_mem.to_csv("pdb_pisa_homo_mem.csv")

homos = list(df_pdbPisa_homo_mem['UNIPROT'])
comp_homos = list(df_pdb['Accession'])

df_pdb_hom = pd.DataFrame({"index":[], "acces": []})

for i, element in enumerate(comp_homos):
    index, acces = "", ""
    if (element in homos):
        index = i
        acces = element
        print(index)
    pdb_ap_hom = [index, acces]
    pdb_series = pd.Series(pdb_ap_hom, index = df_pdb_hom.columns)
    df_pdb_hom = df_pdb_hom.append(pdb_series, ignore_index=True)    
    
df_pdb_hom = df_pdb_hom.drop(df_pdb_hom[df_pdb_hom['acces']==""].index)

index_homo = list(df_pdb_hom['index'])

df_pdbPisa_homodimer = pandas.DataFrame(df_pdb, index = index_homo)

df_pdbPisa_homodimer = df_pdbPisa_homodimer.drop(['ID', 'Gene', 'Organism', 'Method', 'Angstrom'], axis=1)

df_pdbPisa_homo_mem = df_pdbPisa_homo_mem.drop(['SEGMENT 1'], axis=1)

df_pdbPisa_homo_mem.columns=['PDB', 'Accession', 'Regio_crist']

df_uniprot_h =df_uniprot.drop(['PDB','Regio_crist'], axis=1)
df_uniprot_h = df_uniprot_h.reindex(columns=['Accession', 'Segments_TM', 'Pfam'])

df_pdbPisa_homo_mem = df_pdbPisa_homo_mem.merge(df_uniprot_h, on="Accession", how="left")

df_pdbPisa_homo_mem.to_csv("pdb_pisa_homo_mem_compl.csv")



df_hetero1 = df_pdbPisa_hetero_mem.drop(['SEGMENT 1.1', 'UNIPROT 2', 'SEGMENT 2.1', 'SEGMENT 2.2'], axis=1)
df_hetero1.columns=['PDB', 'Accession', 'Regio_crist']
df_hetero1 = df_hetero1.merge(df_uniprot_h, on="Accession", how="left")
df_hetero1.columns=['PDB', 'Accession_1', 'Regio_crist_1', 'Segments_TM_1', 'Pfam1']


df_hetero2 = df_pdbPisa_hetero_mem.drop(['PDBs', 'UNIPROT 1', 'SEGMENT 1.1', 'SEGMENT 1.2','SEGMENT 2.1'], axis=1)
df_hetero2.columns=[ 'Accession', 'Regio_crist']
df_hetero2 = df_hetero2.merge(df_uniprot_h, on="Accession", how="left")
df_hetero2.columns=['Accession_2', 'Regio_crist_2', 'Segments_TM_2', 'Pfam2']


df_hetero_mem = df_hetero1.merge(df_hetero2, how='left', left_index=True, right_index=True)
df_hetero_mem.to_csv("pdb_pisa_hetero_mem_compl.csv")

de_hetero_homo = df_hetero_mem[df_hetero_mem['Accession_2']==""]
df_hetero_mem_def = df_hetero_mem.drop(df_hetero_mem[df_hetero_mem['Accession_2']==""].index)

regio = list(df_hetero_mem_def['Regio_crist_1'])
tm = list(df_hetero_mem_def['Segments_TM_1'])
index_hetero_1 = []
for i, element in enumerate(regio):
    r = regio[i]
    tmi = list(tm[i])
    tm1 =tmi[0].split("..")
    tm2 = tmi[len(tmi)-1].split("..")
    if (int(r[1]) > int(tm1[0])):
        if (int(r[0]) < int(tm2[1])):
            index_hetero_1.append(i)
    
df_hetero_mem_def.index= range(len(df_hetero_mem_def))
df_hetero_mem_def = pandas.DataFrame(df_hetero_mem_def, index = index_hetero_1)
df_hetero_mem_def.index= range(len(df_hetero_mem_def))

regio2 = list(df_hetero_mem_def['Regio_crist_2'])
tm22 = list(df_hetero_mem_def['Segments_TM_2'])
index_hetero_2 = []
for i, element in enumerate(regio2):
    r = regio2[i]
    tmi = list(tm22[i])
    tm1 =tmi[0].split("..")
    tm2 = tmi[len(tmi)-1].split("..")
    print(i)
    if (int(r[1]) > int(tm1[0])):
        if (int(r[0]) < int(tm2[1])):
            index_hetero_2.append(i)

df_hetero_mem_def = pandas.DataFrame(df_hetero_mem_def, index = index_hetero_2)
df_hetero_mem_def.index= range(len(df_hetero_mem_def))
df_hetero_mem_def.to_csv("pdb_pisa_hetero_mem_compl_def.csv")

de_hetero_homo = de_hetero_homo.drop(['Accession_2', 'Regio_crist_2', 'Segments_TM_2', 'Pfam2'], axis=1)
de_hetero_homo.columns=['PDB', 'Accession', 'Regio_crist', 'Segments_TM', 'Pfam']

df_pdbPisa_homo_mem_def = df_pdbPisa_homo_mem.append(de_hetero_homo, ignore_index=True)

regio = list(df_pdbPisa_homo_mem_def['Regio_crist'])
tm = list(df_pdbPisa_homo_mem_def['Segments_TM'])
index_homo_1 = []
for i, element in enumerate(regio):
    r = regio[i]
    tmi = list(tm[i])
    tm1 =tmi[0].split("..")
    tm2 = tmi[len(tmi)-1].split("..")
    print(i)
    if r[1]=='\n':
        print(r)
    else:
        if (int(r[1]) > int(tm1[0])):
            if (int(r[0]) < int(tm2[1])):
                index_homo_1.append(i)
    
df_pdbPisa_homo_mem_def = pandas.DataFrame(df_pdbPisa_homo_mem_def, index = index_homo_1)
df_pdbPisa_homo_mem_def.index= range(len(df_pdbPisa_homo_mem_def))
df_pdbPisa_homo_mem_def.to_csv("pdb_pisa_homo_mem_compl_def.csv")
